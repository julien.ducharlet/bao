var aOfProducts = [];
var success = 0;
var error = 0;
var message = "";
var tablesProductAvailableDate;
var documentRoot;

function loadDocumentRoot() {
    var datas = {
        path : "utils",
        page : "get_document_root",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            documentRoot = result;
            var cron_command_div = document.getElementById("cron_command");
            cron_command_div.innerHTML = "Pour lancer le cron, utilisez la commande suivante : /usr/local/bin/php " + documentRoot + "/modules/boiteaoutils/files/cron_product_available_date/cron/cron_product_available_date.php";
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

function getProductAvailableDate() {
    div_cron_product_available_alert_success = document.getElementById("cron_product_available_alert_success");
    $('#divModalLoading').show();
    var datas = {
        path : "cron_product_available_date",
        page: "cron_product_available_date_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iProduct = 0;
            for (var ligne in result) {
                aOfProducts[iProduct] = [];
                aOfProducts[iProduct]["id_product"] = result[ligne]["id_product"];
                aOfProducts[iProduct]["name"] = result[ligne]["name"];
                aOfProducts[iProduct]["available_date"] = result[ligne]["available_date"];
                iProduct++;
            }
            if (aOfProducts.length != 0) {
                // INIT DATATABLE
                constructTableProductAvailableDate();
                // reload datatable configuration
                tablesProductAvailableDate = $('#datatable').DataTable(configuration);
            }
            else {
                div_cron_product_available_alert_success.innerHTML += "<strong>Aucun produit n'a de date de disponibilité dépassée</strong><br/>";
                div_cron_product_available_alert_success.style.display = "block";
            }
            $('#divModalLoading').hide();
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}


function updateProductAvailableDate() {
    var div_cron_product_available_alert_success = document.getElementById("cron_product_available_alert_success");
    for (var i = 0; i < aOfProducts.length; i++) {
        var datas = {
            path : "cron_product_available_date",
            page: "cron_product_available_date_update",
            bJSON: 1,
            'id_product': aOfProducts[i]["id_product"]
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    for (var i = 0; i < aOfProducts.length; i++) {
        div_cron_product_available_alert_success.innerHTML += "La date " + aOfProducts[i]["available_date"] + " du produit " + aOfProducts[i]["id_product"] + " a été réinitialisée" + "<br/>";
    }
    if (aOfProducts.length == 0) {
        div_cron_product_available_alert_success.innerHTML += "Aucune date n'a été réinitialisée";
    }
    div_cron_product_available_alert_success.style.display = "block";
}

// construit la table avec les données
function constructTableProductAvailableDate() {
    var i;
    // tableau datatable
    var sHTML = "";

    sHTML += '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th>ID</th>";
    sHTML += "<th>Nom</th>";
    sHTML += "<th>Date dispo</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfProducts.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += "<td>" + aOfProducts[i]["id_product"] + "</td>";

        sHTML += "<td>" + aOfProducts[i]["name"] + "</td>";

        sHTML += "<td>" + aOfProducts[i]["available_date"] + "</td>";

        sHTML += "</tr>";
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#product_avalaible_date_table').html(sHTML);
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableProductAvailableDate() {
    tablesProductAvailableDate.clear();
    tablesProductAvailableDate.destroy();
    constructTableProductAvailableDate();
    tablesProductAvailableDate = $('#datatable').DataTable(configuration);
}


// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
// orderable : triable (true ou false)
// visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
// searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[0, "asc"]],
    "pageLength": 10,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        }
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

$(document).ready(function() {
    getProductAvailableDate();
    loadDocumentRoot();
});