<?php

$included_files = get_included_files();
$path_parts = explode("/", $included_files[0]);
$i = 0;

while (($path_parts[$i] != 'modules') && ($i < count($path_parts))) {
    $path .= $path_parts[$i] . "/";
    $i++;
}

$DB_infos = require $path . 'app/config/parameters.php';

$servername = $DB_infos["parameters"]["database_host"];
$username = $DB_infos["parameters"]["database_user"];
$password = $DB_infos["parameters"]["database_password"];
$dbname = $DB_infos["parameters"]["database_name"];

$conn = new mysqli($servername, $username, $password,$dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$message = "";

$result = mysqli_query($conn,"SELECT `id_product`, `available_date`
FROM
`PB_product_shop`
WHERE `available_date` < NOW() AND `available_date` <> 0000-00-00;");

$i = 0;
while ($row = $result->fetch_assoc())
{
    $products_to_update[$i] = [];
    $products_to_update[$i]["id_product"] = $row["id_product"];
    $products_to_update[$i]["available_date"] = $row["available_date"];
    $i++;
}

for ($i = 0; $i < count($products_to_update); $i++) {
    $sql = "UPDATE `PB_product_shop` SET `available_date`=0000-00-00 WHERE `id_product`= " . $products_to_update[$i]["id_product"];

    if ($conn->query($sql) !== TRUE) {
        $message .= "le produit " . $products_to_update[$i]["id_product"] . " à la date " . $products_to_update[$i]["available_date"] . "n'a pas pu être mis à jour" . "\n";
    }
}

$result = mysqli_query($conn,"SELECT `value` FROM `PB_configuration` WHERE `name` = 'PS_SHOP_EMAIL';");
while ($row = $result->fetch_assoc())
{
    $mail = $row["value"];
}

if ($message != "") {
    mail($mail, 'CRON products avalaible date', $message);
}

?>