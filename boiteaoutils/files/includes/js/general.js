/**
 * detect IE
 * returns version of IE or false, if browser is not Internet Explorer or Edge
 */
function detectIEorSafari() {
    var ua = window.navigator.userAgent;

    var msie = ua.indexOf('MSIE ');
    if (msie > 0) {
        // IE 10 or older
        return true;
    }

    var trident = ua.indexOf('Trident/');
    if (trident > 0) {
        // IE 11
        return true;
    }

    var edge = ua.indexOf('Edge/');
    if (edge > 0) {
        // Edge (IE 12+)
        return true;
    }

    var safari = ua.indexOf('Safari/');
    var chrome = ua.indexOf('Chrome/');
    if ((safari > 0) && (chrome == -1)) {
        // Safari
        return true;
    }

    // other browser
    return false;
}

/**
 * Convert date aaaa-mm-jj into jj/mm/aaaa
 */
function convertDate(sDate)	{
    var aOfDates= sDate.split("-");
    return aOfDates[2] + "/" + aOfDates[1] + "/" + aOfDates[0];
}

/**
 * Convert date jj/mm/aaaa into aaaa-mm-jj
 */
function inverseDate(sDate)	{
    var aOfDates= sDate.split("/");
    return aOfDates[2] + "-" + aOfDates[1] + "-" + aOfDates[0];
}

/**
 * Convert specials HTML entities HTML in character
 */
function htmlspecialchars_decode(str) {
    if (typeof(str) == "string") {
        str = str.replace(/&amp;/g, "&");
        str = str.replace(/&quot;/g, "\"");
        str = str.replace(/&#039;/g, "'");
        str = str.replace(/&lt;/g, "<");
        str = str.replace(/&gt;/g, ">");
    }
    return str;
}