var tokenAdminProducts;

/**
 * public aOfFilms is used to store all datas of movies
 * @var array
 */
var aOfAdminStocks = [];

// variable contenant la datatable
var tablesAdminStocks;

var iAdminStocksMax;

function loadTokenAdminProducts() {
    var datas = {
        path : "token",
        page : "get_token_admin_products",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var linkAdminProducts = result;
            var AdminProductsParts = linkAdminProducts.split('token=', 2);
            tokenAdminProducts = AdminProductsParts[1];
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

/**
 * Get Movies from database
 *
 * if OK add movies to array aOfFilms
 *
 * if OK then build table and call datatable
 */
// charge la liste des stocks en Ajax et construit le tableau JS
function loadAdminStocks() {
    $('#divModalLoading').show();
    var datas = {
        path : "admin_stock",
        page : "admin_stock_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var iAdminStock = 0;
            for (var ligne in result)	{
                aOfAdminStocks[iAdminStock] = [];
                aOfAdminStocks[iAdminStock]["product_id"] = result[ligne]["product_id"];
                aOfAdminStocks[iAdminStock]["reference"] = result[ligne]["reference"];
                aOfAdminStocks[iAdminStock]["product_attribute_id"] = result[ligne]["product_attribute_id"];
                aOfAdminStocks[iAdminStock]["id_image"] = result[ligne]["id_image"];
                aOfAdminStocks[iAdminStock]["link_rewrite"] = result[ligne]["link_rewrite"];
                aOfAdminStocks[iAdminStock]["product_name"] = result[ligne]["product_name"];
                aOfAdminStocks[iAdminStock]["product_attribute_group_name"] = result[ligne]["product_attribute_group_name"];
                aOfAdminStocks[iAdminStock]["product_attribute_name"] = result[ligne]["product_attribute_name"];
                aOfAdminStocks[iAdminStock]["sales_stats_duration_1"] = result[ligne]["sales_stats_duration_1"];
                aOfAdminStocks[iAdminStock]["sales_stats_duration_2"] = result[ligne]["sales_stats_duration_2"];
                aOfAdminStocks[iAdminStock]["sales_stats_duration_3"] = result[ligne]["sales_stats_duration_3"];
                aOfAdminStocks[iAdminStock]["sales_stats_duration_all"] = result[ligne]["sales_stats_duration_all"];
                aOfAdminStocks[iAdminStock]["low_stock_threshold"] = result[ligne]["low_stock_threshold"];
                aOfAdminStocks[iAdminStock]["ideal_stock"] = result[ligne]["ideal_stock"];
                aOfAdminStocks[iAdminStock]["stock_ideal_conseille"] = result[ligne]["stock_ideal_conseille"];
                iAdminStock++;
            }
            iAdminStockMax = iAdminStock;
            // INIT DATATABLE
            constructTableAdminStocks();
            // reload datatable configuration
            tablesAdminStocks = $('#datatable').DataTable(configuration);
            $('#divModalLoading').hide();
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

function updateIdealStock(iAdminStock, ideal_stock) {
    var idealStock = document.getElementById("input_ideal_stock_" + iAdminStock).value;
    var div_td_ideal_stock = document.getElementById("td_ideal_stock_" + iAdminStock);
    var div_alert = document.getElementById("admin_stock_alert");
    $('#divModalLoading').show();

    var datas = {
        path : "admin_stock",
        page : "admin_stock_update_ideal_stock",
        bJSON : 1,
        product_id : aOfAdminStocks[iAdminStock]["product_id"],
        product_attribute_id : aOfAdminStocks[iAdminStock]["product_attribute_id"],
        ideal_stock : idealStock
    }

    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            if (result[0]["error_admin_stock_update_bao_product_ideal_stock"] != "") {
                div_td_ideal_stock.style.backgroundColor = "#CE5454";
                $('#divModalLoading').hide();
            } else {
                div_td_ideal_stock.style.backgroundColor= "#26B99A";
                aOfAdminStocks[iAdminStock]["ideal_stock"] = document.getElementById("input_ideal_stock_" + iAdminStock).value;
                $('#divModalLoading').hide();
            }
        })
        .fail(function (err) {
            console.log('error : ' + err.status);
            div_td_ideal_stock.style.backgroundColor = "#CE5454";
            $('#divModalLoading').hide();
        });
}

function updateLowStockThreshold(iAdminStock, low_stock_threshold) {
    var lowStockThreshold = document.getElementById("input_low_stock_threshold_" + iAdminStock).value;
    var div_td_low_stock_threshold = document.getElementById("td_low_stock_threshold_" + iAdminStock);
    var div_alert = document.getElementById("admin_stock_alert");
    var error_update_lowstock = 0;

    var datas = {
        path : "admin_stock",
        page : "admin_stock_update_low_stock_threshold",
        bJSON : 1,
        id_product : aOfAdminStocks[iAdminStock]["product_id"],
        id_product_attribute : aOfAdminStocks[iAdminStock]["product_attribute_id"],
        low_stock_threshold : lowStockThreshold
    }

    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            if (aOfAdminStocks[iAdminStock]["product_attribute_id"] == 0) {
                if (result[0]["error_admin_stock_update_product_low_stock_threshold"] != "") {
                    error_update_lowstock = 1;
                }
                if (result[0]["error_admin_stock_update_product_shop_low_stock_threshold"] != "") {
                    error_update_lowstock = 2;
                }
            } else {
                if (result[0]["error_admin_stock_update_product_attribute_low_stock_threshold"] != "") {
                    error_update_lowstock = 3;
                }
            }
            if (error_update_lowstock != 0) {
                console.log(error_update_lowstock);
                div_td_low_stock_threshold.style.backgroundColor = "#CE5454";
                $('#divModalLoading').hide();
            }
            else {
                div_td_low_stock_threshold.style.backgroundColor= "#26B99A";
                aOfAdminStocks[iAdminStock]["low_stock_threshold"] = document.getElementById("input_low_stock_threshold_" + iAdminStock).value;
                $('#divModalLoading').hide();
            }
        })
        .fail(function (err) {
            console.log('error : ' + err.status);
            div_td_low_stock_threshold.style.backgroundColor = "#CE5454";
            $('#divModalLoading').hide();
        });
}

// construit la table avec les données
function constructTableAdminStocks() {
    var i;
    // tableau datatable
    var sHTML = "";

    sHTML += '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th style='padding-right: 1.0em !important;'>ID</th>";
    sHTML += "<th>REFERENCE</th>";
    sHTML += "<th>Image</th>";
    sHTML += "<th>Nom</th>";
    sHTML += "<th>Nb ventes<br/>sur 6 mois</th>";
    sHTML += "<th>Nb ventes<br/>sur 2 mois</th>";
    sHTML += "<th>Nb ventes<br/>sur 1 mois</th>";
    sHTML += "<th>Nb ventes<br/>totales</th>";
    sHTML += "<th>Niveau de stock bas</th>";
    sHTML += "<th>Stock idéal</th>";
    sHTML += "<th>Stock idéal conseillé sur 20j</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";

    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfAdminStocks.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += '<th scope="row" style="text-align: center;"><a href="https://www.pincab.eu/admin797xdo6aw/index.php/sell/catalog/products/' + aOfAdminStocks[i]["product_id"] + '?_token=' + tokenAdminProducts + '#tab-step6' + '" ' + 'target="_blank">' + aOfAdminStocks[i]["product_id"] + '</a></th>';

        sHTML += "<td>" + aOfAdminStocks[i]["reference"] + "</td>";

        sHTML += '<td>' + '<img src="https://www.pincab.eu/' + aOfAdminStocks[i]["id_image"] + '-small_default/' + aOfAdminStocks[i]["link_rewrite"] + '.jpg' + '" height="46px" width="46px">' + '</td>';

        if ((aOfAdminStocks[i]["product_attribute_group_name"] !== "") && (aOfAdminStocks[i]["product_attribute_name"] !== "")) {
            sHTML += "<td>" + aOfAdminStocks[i]["product_name"] + "<br/><p style='font-weight: bold;'>" + aOfAdminStocks[i]["product_attribute_group_name"] + " : " + aOfAdminStocks[i]["product_attribute_name"] + "</p></td>";
        }
        else {
            sHTML += "<td>" + aOfAdminStocks[i]["product_name"] + "</td>";
        }

        sHTML += "<td>" + aOfAdminStocks[i]["sales_stats_duration_3"] + "</td>";

        sHTML += "<td>" + aOfAdminStocks[i]["sales_stats_duration_2"] + "</td>";

        sHTML += "<td>" + aOfAdminStocks[i]["sales_stats_duration_1"] + "</td>";

        sHTML += "<td>" + aOfAdminStocks[i]["sales_stats_duration_all"] + "</td>";

        sHTML += `<td id="td_low_stock_threshold_` + i + `"><input tabindex="` + (i + 1) + `" type="number" min="0" id="input_low_stock_threshold_` + i + `" name="input_low_stock_threshold_` + i + `" value="` + aOfAdminStocks[i]["low_stock_threshold"] + `"onfocusout="updateLowStockThreshold(` + i + `,` + aOfAdminStocks[i]["low_stock_threshold"] + `)"></td>`;

        sHTML += `<td id="td_ideal_stock_` + i + `"><input tabindex="` + (iAdminStockMax + i) + `" type="number" min="0" id="input_ideal_stock_` + i + `" name="input_ideal_stock_` + i + `" value="` + aOfAdminStocks[i]["ideal_stock"] + `"onfocusout="updateIdealStock(` + i + `,` + aOfAdminStocks[i]["ideal_stock"] + `)"></td>`;

        sHTML += "<td>" + aOfAdminStocks[i]["stock_ideal_conseille"]  + "</td>";

        sHTML += "</tr>";
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#admin_stock_table').html(sHTML);
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableAdminStocks() {
    tablesAdminStocks.clear();
    tablesAdminStocks.destroy();
    constructTableAdminStocks();
    tablesAdminStocks = $('#datatable').DataTable(configuration);
}

// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
    // orderable : triable (true ou false)
    // visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
    // searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[0, "asc"]],
    "pageLength": 100,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": true
        },
        {
            "visible": false
        },
        {
            "orderable": false
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": false
        },
        {
            "orderable": false
        },
        {
            "orderable": true
        }
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

// une fois la page chargée, charger les stocks
$(document).ready(function() {
    loadTokenAdminProducts();
    loadAdminStocks();
});