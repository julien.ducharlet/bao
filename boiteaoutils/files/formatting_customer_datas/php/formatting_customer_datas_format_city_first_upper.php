<?php
require_once "formatting_customer_datas_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Formatting_customer_datas_format_city_first_upper	{

    /**
     * public $resultat is used to store all datas needed for HTML Templates
     * @var array
     */
    public $resultat;

    /**
     * init variables resultat
     *
     * execute main function
     */
    public function __construct()	{
        // init variables resultat
        $this->resultat = [];

        // execute main function
        $this->main();
    }

    /**
     * Update a movie with its id
     */
    function main()	{
        $objet_formatting_customer_datas_format_city_first_upper = new Formatting_customer_datas_service();
        $objet_formatting_customer_datas_format_city_first_upper->formatting_customer_datas_format_city_first_upper();

        $this->resultat = $objet_formatting_customer_datas_format_city_first_upper->resultat;
        $this->VARS_HTML = $objet_formatting_customer_datas_format_city_first_upper->VARS_HTML;
    }
}
?>
