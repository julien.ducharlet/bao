var documentRoot;

function loadDocumentRoot() {
    var datas = {
        path : "utils",
        page : "get_document_root",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            documentRoot = result;
            var cron_command_div = document.getElementById("cron_command");
            cron_command_div.innerHTML = "Pour lancer le cron, utilisez la commande suivante : /usr/local/bin/php " + documentRoot + "/modules/boiteaoutils/files/formatting_customer_datas/cron/cron_formatting_customer_datas.php";
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}


function formatClient() {
    var div_gestion_client_alert_success = document.getElementById("gestion_client_alert_success");
    var div_gestion_client_alert_failed = document.getElementById("gestion_client_alert_failed");
    div_gestion_client_alert_success.innerHTML = "";
    div_gestion_client_alert_failed.innerHTML = "";

    var success = 0;
    var failed = 0;

    var last_name = document.getElementsByName('last_name');
    var first_name = document.getElementsByName('first_name');
    var company = document.getElementsByName('company');
    var address = document.getElementsByName('address');
    var city = document.getElementsByName('city');

    for (var i = 0, length = last_name.length; i < length; i++) {
        if (last_name[i].checked) {
            last_name_value = last_name[i].value;
            break;
        }
    }
    for (var i = 0, length = first_name.length; i < length; i++) {
        if (first_name[i].checked) {
            var first_name_value = first_name[i].value;
            break;
        }
    }
    for (var i = 0, length = company.length; i < length; i++) {
        if (company[i].checked) {
            var company_value = company[i].value;
            break;
        }
    }
    for (var i = 0, length = address.length; i < length; i++) {
        if (address[i].checked) {
            var address_value = address[i].value;
            break;
        }
    }
    for (var i = 0, length = city.length; i < length; i++) {
        if (city[i].checked) {
            var city_value = city[i].value;
            break;
        }
    }

    if (last_name_value == 0) {
        success++;
    } else if (last_name_value == 1) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_last_name_all_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_last_name_all_upper_address"] != "") || (result[0]["error_formatting_customer_datas_format_last_name_all_upper_customer"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    } else if (last_name_value == 2) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_last_name_first_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_last_name_first_upper_address"] != "") || (result[0]["error_formatting_customer_datas_format_last_name_first_upper_customer"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    }

    if (first_name_value == 0) {
        success++;
    } else if (first_name_value == 1) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_first_name_all_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_first_name_all_upper_address"] != "") || (result[0]["error_formatting_customer_datas_format_first_name_all_upper_customer"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    } else if (first_name_value == 2) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_first_name_first_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_first_name_first_upper_address"] != "") || (result[0]["error_formatting_customer_datas_format_first_name_first_upper_customer"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    }

    if (company_value == 0) {
        success++;
    } else if (company_value == 1) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_company_all_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if (result[0]["error_formatting_customer_datas_format_company_all_upper"] != "") {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    } else if (company_value == 2) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_company_first_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if (result[0]["error_formatting_customer_datas_format_company_first_upper"] != "") {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    }

    if (address_value == 0) {
        success++;
    } else if (address_value == 1) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_adress_all_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_adress_1_all_upper"] != "") || (result[0]["error_formatting_customer_datas_format_adress_2_all_upper"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    } else if (address_value == 2) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_adress_first_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if ((result[0]["error_formatting_customer_datas_format_adress_1_first_upper"] != "") || (result[0]["error_formatting_customer_datas_format_adress_2_first_upper"] != "")) {
                    failed++;
                } else {
                    success++;
                }
            })
            .fail(function (err) {
                failed++;
            });
    }

    if (city_value == 0) {
        success++;
    } else if (city_value == 1) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_city_all_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if (result[0]["error_formatting_customer_datas_format_city_all_upper"] != "") {
                    failed++;
                } else {
                    success++;
                    div_gestion_client_alert_success.innerHTML += "<strong>Le formatage a bien été pris en compte</strong><br/>";
                    div_gestion_client_alert_success.style.display = "block";
                }
            })
            .fail(function (err) {
                failed++;
            });
    } else if (city_value == 2) {
        var datas = {
            path: "formatting_customer_datas",
            page: "formatting_customer_datas_format_city_first_upper",
            bJSON: 1
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                console.log(result);
                if (result[0]["error_formatting_customer_datas_format_city_first_upper"] != "") {
                    failed++;
                } else {
                    success++;
                    div_gestion_client_alert_success.innerHTML += "<strong>Le formatage a bien été pris en compte</strong><br/>";
                    div_gestion_client_alert_success.style.display = "block";
                }
            })
            .fail(function (err) {
                failed++;
            });
    }
}

function loadConfigFormatClient() {
    var datas = {
        path: "formatting_customer_datas",
        page: "formatting_customer_datas_load_config",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var last_name_option = result['last_name'];
            var first_name_option = result['first_name'];
            var company_option = result['company'];
            var address_option = result['address'];
            var city_option = result['city'];

            var last_name_input = '#last_name_option_' + last_name_option;
            var first_name_input = '#first_name_option_' + first_name_option;
            var company_input = '#company_option_' + company_option;
            var address_input = '#address_option_' + address_option;
            var city_input = '#city_option_' + city_option;


            $(last_name_input).iCheck('check');
            $(first_name_input).iCheck('check');
            $(company_input).iCheck('check');
            $(address_input).iCheck('check');
            $(city_input).iCheck('check');

        })
        .fail(function(err) {
            console.log("error");
        });
}

function saveConfigFormatClient() {
    var div_gestion_client_alert_success = document.getElementById("gestion_client_alert_success");
    var last_name = document.getElementsByName('last_name');
    var first_name = document.getElementsByName('first_name');
    var company = document.getElementsByName('company');
    var address = document.getElementsByName('address');
    var city = document.getElementsByName('city');

    for (var i = 0, length = last_name.length; i < length; i++) {
        if (last_name[i].checked) {
            last_name_value = last_name[i].value;
            break;
        }
    }
    for (var i = 0, length = first_name.length; i < length; i++) {
        if (first_name[i].checked) {
            var first_name_value = first_name[i].value;
            break;
        }
    }
    for (var i = 0, length = company.length; i < length; i++) {
        if (company[i].checked) {
            var company_value = company[i].value;
            break;
        }
    }
    for (var i = 0, length = address.length; i < length; i++) {
        if (address[i].checked) {
            var address_value = address[i].value;
            break;
        }
    }
    for (var i = 0, length = city.length; i < length; i++) {
        if (city[i].checked) {
            var city_value = city[i].value;
            break;
        }
    }

    var datas = {
        path: "formatting_customer_datas",
        page: "formatting_customer_datas_save_config",
        last_name : last_name_value,
        first_name : first_name_value,
        company : company_value,
        address : address_value,
        city : city_value,
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            console.log(result);
            if (result[0]["error_formatting_customer_datas_save_config"] != "") {
                console.log("error 1");
                div_gestion_client_alert_success.innerHTML += "<strong>Vos paramètres ont bien été sauvegardés</strong><br/>";
                div_gestion_client_alert_success.style.display = "block";
            } else {
                console.log("success");
            }
        })
        .fail(function (err) {
            console.log("error 2");
        });
}

// une fois la page chargée, charger les commandes
$(document).ready(function() {
    loadDocumentRoot();
    loadConfigFormatClient();
});
