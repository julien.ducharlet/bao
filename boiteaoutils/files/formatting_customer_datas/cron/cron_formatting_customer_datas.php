<?php

$included_files = get_included_files();
$path_parts = explode("/", $included_files[0]);
$i = 0;

while (($path_parts[$i] != 'modules') && ($i < count($path_parts))) {
    $path .= $path_parts[$i] . "/";
    $i++;
}

$DB_infos = require $path . 'app/config/parameters.php';

$servername = $DB_infos["parameters"]["database_host"];
$username = $DB_infos["parameters"]["database_user"];
$password = $DB_infos["parameters"]["database_password"];
$dbname = $DB_infos["parameters"]["database_name"];

$success = 0;
$error = 0;

$conn = new mysqli($servername, $username, $password, $dbname);

if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

$lines = file($path . "modules/boiteaoutils/files/formatting_customer_datas/formatting_customer_datas_config.txt");

for ($i = 0; $i < count($lines); $i++) {
    if (preg_match("/\blast_name\b/i", $lines[$i], $match)) {
        $last_name_explode = explode('=',$lines[$i]);
        $last_name_option = $last_name_explode[1];
    }
    if (preg_match("/\bfirst_name\b/i", $lines[$i], $match)) {
        $first_name_explode = explode('=',$lines[$i]);
        $first_name_option = $first_name_explode[1];
    }
    if (preg_match("/\bcompany\b/i", $lines[$i], $match)) {
        $company_explode = explode('=',$lines[$i]);
        $company_option = $company_explode[1];
    }
    if (preg_match("/\baddress\b/i", $lines[$i], $match)) {
        $address_explode = explode('=',$lines[$i]);
        $address_option = $address_explode[1];
    }
    if (preg_match("/\bcity\b/i", $lines[$i], $match)) {
        $city_explode = explode('=',$lines[$i]);
        $city_option = $city_explode[1];
    }
}

if ($last_name_option == 1) {
    $sql_last_name_1_1 = "UPDATE PB_customer SET PB_customer.lastname = UPPER(PB_customer.lastname);";

    if ($conn->query($sql_last_name_1_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_last_name_1_2 = "UPDATE PB_address SET PB_address.lastname = UPPER(PB_address.lastname);";

    if ($conn->query($sql_last_name_1_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}
else if ($last_name_option == 2) {
    $sql_last_name_2_1 = "UPDATE PB_customer SET PB_customer.lastname = CONCAT(UPPER(LEFT(PB_customer.lastname,1)), LOWER(SUBSTRING(PB_customer.lastname,2)));";

    if ($conn->query($sql_last_name_2_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_last_name_2_2 = "UPDATE PB_address SET PB_address.lastname = CONCAT(UPPER(LEFT(PB_address.lastname,1)), LOWER(SUBSTRING(PB_address.lastname,2)));";

    if ($conn->query($sql_last_name_2_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}

if ($first_name_option == 1) {
    $sql_first_name_1_1 =  "UPDATE PB_customer SET PB_customer.firstname = UPPER(PB_customer.firstname);";

    if ($conn->query($sql_first_name_1_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_first_name_1_2 =  "UPDATE PB_address SET PB_address.firstname = UPPER(PB_address.firstname);";

    if ($conn->query($sql_first_name_1_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}
else if ($first_name_option == 2) {
    $sql_first_name_2_1 = "UPDATE PB_customer SET PB_customer.firstname = UC_Words(PB_customer.firstname);";

    if ($conn->query($sql_first_name_2_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_first_name_2_2 = "UPDATE PB_address SET PB_address.firstname = UC_Words(PB_address.firstname);";

    if ($conn->query($sql_first_name_2_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}

if ($company_option == 1) {
    $sql_company_1 = "UPDATE PB_address SET PB_address.company = UPPER(PB_address.company);";

    if ($conn->query($sql_company_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}
else if ($company_option == 2) {
    $sql_company_2 = "UPDATE PB_address SET PB_address.company = CONCAT(UPPER(LEFT(PB_address.company,1)), LOWER(SUBSTRING(PB_address.company,2)));";

    if ($conn->query($sql_company_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}

if ($address_option == 1) {
    $sql_address_1_1 =  "UPDATE PB_address SET PB_address.address1 = UPPER(PB_address.address1);";

    if ($conn->query($sql_address_1_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_address_1_2 = "UPDATE PB_address SET address2 = UPPER(address2);";

    if ($conn->query($sql_address_1_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

}
else if ($address_option == 2) {
    $sql_address_2_1 = "UPDATE PB_address SET PB_address.address1 = CONCAT(UPPER(LEFT(PB_address.address1,1)), LOWER(SUBSTRING(PB_address.address1,2)));";

    if ($conn->query($sql_address_2_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }

    $sql_address_2_2 = "UPDATE PB_address SET PB_address.address2 = CONCAT(UPPER(LEFT(PB_address.address2,1)), LOWER(SUBSTRING(PB_address.address2,2)));";

    if ($conn->query($sql_address_2_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}

if ($city_option == 1) {
    $sql_city_1 = "UPDATE PB_address SET PB_address.city = UPPER(PB_address.city);";

    if ($conn->query($sql_city_1) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}
else if ($city_option == 2) {
    $sql_city_2 = "UPDATE PB_address SET PB_address.city = CONCAT(UPPER(LEFT(PB_address.city,1)), LOWER(SUBSTRING(PB_address.city,2)));";

    if ($conn->query($sql_city_2) === TRUE) {
        $success++;
    }
    else {
        $error++;
    }
}

$result = mysqli_query($conn,"SELECT `value` FROM `PB_configuration` WHERE `name` = 'PS_SHOP_EMAIL';");
while ($row = $result->fetch_assoc())
{
    $mail = $row["value"];
}

if ($error != 0) {
    $message = "Une erreur s'est produite";
    mail($mail, 'CRON formatting customer datas', $message);
}

?>