// une fois la page chargée, charger les commandes
$(document).ready(function() {
    var inputDateAdd = document.getElementById("date_add");
    var date = new Date();
    inputDateAdd.value = date.getFullYear().toString() + '-' + (date.getMonth() + 1).toString().padStart(2, 0) +
        '-' + date.getDate().toString().padStart(2, 0);

    getShops();
});

var idShop;
var aOfProducts = [];
var aOfIdProductsPassed = [];
var aOfIdProductsNotPassed = [];
var aOfidProductsToCheck = [];
var idProductsToCheck;
var confirmUpdateDateAdd;

function getShops() {
    var datas = {
        path : "update_new_products",
        page : "update_new_products_shop_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            for (var ligne in result)	{
                $('#shop_select').append($('<option>', {
                    value: result[ligne]["id_shop"],
                    text: result[ligne]["name_shop"]
                }));
            }
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

function checkIdProducts() {
    confirmUpdateDateAdd = false;
    aOfProducts = [];
    aOfIdProductsPassed = [];
    aOfIdProductsNotPassed = [];
    aOfidProductsToCheck = [];
    idShop = $("#shop_select").val();
    console.log(idShop);
    if ($("#update_product_date_add").is(":checked")) {
        confirmUpdateDateAdd = confirm("Etes-vous sûr de vouloir mettre à jour la date du (des) produit(s) dans la table products ?");
    }

    idProductsToCheck = document.getElementById("id_products").value;
    aOfidProductsToCheck = idProductsToCheck.split(",");
    var datas = {
        path : "update_new_products",
        page : "update_new_products_id_product_list",
        bJSON : 1,
        id_shop : idShop
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            var found = 0;
            console.log(result);
            for (i = 0; i < aOfidProductsToCheck.length; i++) {
                for (var ligne in result) {
                    if (found == 0) {
                        if (aOfidProductsToCheck[i] == result[ligne]["id_product"]) {
                            aOfIdProductsPassed.push(aOfidProductsToCheck[i]);
                            found = 1;
                        }
                    }
                }
                if (found != 1) {
                    aOfIdProductsNotPassed.push(aOfidProductsToCheck[i]);
                }
                found = 0;
            }
            if (aOfIdProductsPassed.length != 0) {
                getDateAdd(aOfIdProductsPassed);
            }
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

function getDateAdd(aOfIdProducts) {
    idProducts = aOfIdProducts.toString();
    console.log("idProducts");
    var datas = {
        path : "update_new_products",
        page : "update_new_products_date_add_list",
        bJSON : 1,
        id_shop : idShop,
        id_products : idProducts
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var iProduct = 0;
            for (var ligne in result)	{
                aOfProducts[iProduct] = [];
                aOfProducts[iProduct]["id_product"] = result[ligne]["id_product"];
                aOfProducts[iProduct]["date_add"] = result[ligne]["date_add"];
                iProduct++;
            }
            if (confirmUpdateDateAdd == true) {
                updateProductDateAdd(aOfProducts);
            }
            updateProductShopDateAdd(aOfProducts);
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

function updateProductDateAdd(aOfProducts) {
    console.log("updateproductdateadd");
    var aOfProductsToUpdate = [];
    var dateAddToUpdate = document.getElementById("date_add").value;
    for (var iProduct in aOfProducts) {
        aOfProductsToUpdate.push(aOfProducts[iProduct]["id_product"]);
    }
    idProductsToUpdate = aOfProductsToUpdate.toString();
    var datas = {
        path : "update_new_products",
        page : "update_new_products_update_product_date_add",
        bJSON : 1,
        id_shop : idShop,
        id_products : idProductsToUpdate,
        date_add : dateAddToUpdate
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            if (result[0]["error_update_new_products_update_product_date_add"] != "") {
                console.log("error");
            }
            else {
                console.log("update OK");
            }
        })
        .fail(function (err) {
            console.log('error : ' + err.status);
        });
}

function updateProductShopDateAdd(aOfProducts) {
    console.log('productupdateshopdateadd');
    var aOfProductsToUpdate = [];
    var AlertDateAddSuccess = document.getElementById("alert_update_date_add_success");
    var AlertDateAddFailed = document.getElementById("alert_update_date_add_failed");
    AlertDateAddSuccess.innerHTML = "";
    AlertDateAddFailed.innerHTML = "";
    var btnAlert = '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>';
    var sSuccess = '';
    var sFailed = '';
    AlertDateAddSuccess.style.display = "none";
    AlertDateAddFailed.style.display = "none";

    var dateAddToUpdate = document.getElementById("date_add").value;
    for (var iProduct in aOfProducts) {
        aOfProductsToUpdate.push(aOfProducts[iProduct]["id_product"]);
    }
    idProductsToUpdate = aOfProductsToUpdate.toString();
    var datas = {
        path : "update_new_products",
        page : "update_new_products_update_product_shop_date_add",
        bJSON : 1,
        id_shop : idShop,
        id_products : idProductsToUpdate,
        date_add : dateAddToUpdate
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            if (result[0]["error_update_new_products_update_product_shop_date_add"] != "") {
                $('#divModalLoading').hide();
                AlertDateAddFailed.innerHTML = btnAlert + "Erreur lors de la mise à jour de la date de vos produits";
                AlertDateAddFailed.style.display = "block";
            } else {
                $('#divModalLoading').hide();
                for (var iProductPassed in aOfProducts) {
                     sSuccess += "La date du produit " + aOfProducts[iProductPassed]["id_product"] + ' est passée de ' + aOfProducts[iProductPassed]["date_add"] + ' à ' + dateAddToUpdate + '<br\>';
                }
                for (var iProductNotPassed in aOfIdProductsNotPassed) {
                    sFailed += "Le produit " + aOfIdProductsNotPassed[iProductNotPassed] + " n'existe pas <br\>";
                }
                AlertDateAddSuccess.innerHTML = btnAlert + sSuccess;
                AlertDateAddSuccess.style.display = "block";

                if (sFailed != '') {
                    AlertDateAddFailed.innerHTML = btnAlert + sFailed;
                    AlertDateAddFailed.style.display = "block";
                }
            }
        })
        .fail(function (err) {
            $('#divModalLoading').hide();
            console.log('error : ' + err.status);
            AlertDateAddFailed.innerHTML = btnAlert + "Erreur lors de la mise à jour de la date de vos produits";
            AlertDateAddFailed.style.display = "block";
        });
}