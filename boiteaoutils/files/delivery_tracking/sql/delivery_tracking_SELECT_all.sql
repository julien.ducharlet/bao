SELECT
    DB_PREFIX_order_carrier.id_order,
    DB_PREFIX_order_carrier.tracking_number,
    DB_PREFIX_orders.current_state,
    DB_PREFIX_carrier.url,
    DB_PREFIX_order_carrier.date_add

FROM `DB_PREFIX_order_carrier`

LEFT JOIN `DB_PREFIX_orders` ON DB_PREFIX_order_carrier.id_order = DB_PREFIX_orders.id_order
LEFT JOIN `DB_PREFIX_carrier` ON DB_PREFIX_order_carrier.id_carrier = DB_PREFIX_carrier.id_carrier

WHERE
    DB_PREFIX_order_carrier.tracking_number IS NOT NULL
    AND DB_PREFIX_order_carrier.tracking_number <> ""
    AND DB_PREFIX_orders.current_state = 4;