<?php
require_once __DIR__ . "/../../includes/php/initialize.php";

/**
 * Class film_service | file film_service.php
 *
 * In this class, we have methods for :
 * - adding a movie with method save_film()
 * - updating a movie with method update_film()
 * - deleting a movie with method supprime_film()
 * - listing all movies with method liste_film()
 * - editing a movie with method edit_film()
 * With this interface, we'll be able to list all the films stored in database
 *
 * List of classes needed for this class
 *
 * require_once "film_service.php";
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Delivery_tracking_service extends Initialize	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * Call the parent constructor
	 *
	 * init variables resultat
	 */
	public function __construct()	{
		// Call Parent Constructor
		parent::__construct();

		// init variables resultat
		$this->resultat= [];
	}

	/**
	 * Call the parent destructor
	 */
	public function __destruct()	{
		// Call Parent destructor
		parent::__destruct();
	}
	
	/**
	 * Method liste_film()
	 *
	 * List all movies in database
	 */
	public function delivery_tracking_list()	{
		$spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'delivery_tracking/' . 'sql/' . "delivery_tracking_SELECT_all.sql";
		$this->resultat["delivery_tracking_list"]= $this->oBdd->getSelectDatas($spathSQL, array());
	}

	/**
	 * Method update_film()
	 *
	 * Update the movie with param id_film in database
	 */
	public function delivery_tracking_update_orders_current_state()	{
		// Here I can Access to :
		// $this->GLOBALS_INI
		// $this->oBdd
		// $this->VARS_HTML
		$spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'delivery_tracking/' . 'sql/' . "delivery_tracking_UPDATE_orders_current_state.sql";
		$this->resultat["delivery_tracking_update_orders_current_state"]= $this->oBdd->treatDatas($spathSQL, array(
			"id_order" => $this->VARS_HTML["id_order_update_status"]
		));
	}

	/**
	 * Method update_film()
	 *
	 * Update the movie with param id_film in database
	 */
	public function delivery_tracking_update_order_history_current_state()	{
		// Here I can Access to :
		// $this->GLOBALS_INI
		// $this->oBdd
		// $this->VARS_HTML
		$spathSQL= $this->GLOBALS_INI["PATH_HOME"] . 'files/' . 'delivery_tracking/' . 'sql/' . "delivery_tracking_UPDATE_order_history_current_state.sql";
		$this->resultat["delivery_tracking_update_order_history_current_state"]= $this->oBdd->treatDatas($spathSQL, array(
			"id_order" => $this->VARS_HTML["id_order_update_status"]
		));
	}
}

?>
