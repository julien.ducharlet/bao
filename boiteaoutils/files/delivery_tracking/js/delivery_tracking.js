var tokenAdminOrders;

/**
 * public aOfFilms is used to store all datas of movies
 * @var array
 */
var aOfOrders = [];

// variable contenant la datatable
var tablesOrders;

function loadTokenAdminOrders() {
    var datas = {
        path : "token",
        page : "get_token_admin_orders",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            tokenAdminOrders = result;
        })
        .fail(function(err) {
            console.log("error");
            // error
        });
}

/**
 * Get Movies from database
 *
 * if OK add movies to array aOfFilms
 *
 * if OK then build table and call datatable
 */
function loadOrders() {
    $('#divModalLoading').show();
    var datas = {
        path : "delivery_tracking",
        page : "delivery_tracking_list",
        bJSON : 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function(result) {
            console.log(result);
            var iOrder = 0;
            for (var ligne in result)	{
                aOfOrders[iOrder] = [];
                aOfOrders[iOrder]["id_order"] = result[ligne]["id_order"];
                aOfOrders[iOrder]["tracking_number"] = htmlspecialchars_decode(result[ligne]["tracking_number"]);
                aOfOrders[iOrder]["url"] = htmlspecialchars_decode(result[ligne]["url"]);
                aOfOrders[iOrder]["current_state"] = result[ligne]["current_state"];
                aOfOrders[iOrder]["date_add"] = result[ligne]["date_add"];
                iOrder++;
            }
            // INIT DATATABLE
            constructTableOrders();
            // reload datatable configuration
            tablesOrders = $('#datatable').DataTable(configuration);
            $('#divModalLoading').hide();
        })
        .fail(function(err) {
            // error
            alert('error : ' + err.status);
        });
}

/**
 * Update a movie in database
 *
 * build table and call datatable
 *
 */
function updateStatus(iUpdateStatusOrder) {
    var idOrderToUpdateStatus = aOfOrders[iUpdateStatusOrder]["id_order"];
    if (confirm('Etes vous sûr de vouloir changer le statut de la commande ' + idOrderToUpdateStatus + '?')) {
        $('#divModalLoading').show();
        var datas = {
            path : "delivery_tracking",
            page : "delivery_tracking_update_current_state",
            bJSON : 1,
            id_order_update_status: idOrderToUpdateStatus
        }

        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function(result) {
                if (result[0]["error_delivery_tracking_update_orders_current_state"] != "") {
                    $('#divModalLoading').hide();
                    alert("Erreur lors de la mise à jour de la commande du statut Expédié à Livré");
                } else if (result[0]["error_delivery_tracking_update_order_history_current_state"] != "") {
                    $('#divModalLoading').hide();
                    alert("Erreur lors de la mise à jour de la commande du statut Expédié à Livré");
                } else {
                    aOfOrders[iUpdateStatusOrder]["current_state"] = 5;
                    rebuildDatatableOrders();
                    $('#divModalLoading').hide();
                }
            })
            .fail(function (err) {
                $('#divModalLoading').hide();
                console.log('error : ' + err.status);
                alert("Erreur lors de la mise à jour de la commande du statut Expédié à Livré");
            });
    }
}

// construit la table avec les données
function constructTableOrders() {
    var i;
    // tableau datatable
    var sHTML = "";

    sHTML += '<table id="datatable" class="table table-striped table-bordered" style="width:100%">';
    sHTML += "<thead>";
    sHTML += "<tr>";
    sHTML += "<th>ID</th>";
    sHTML += "<th>Numéro de suivi</th>";
    sHTML += "<th>Statut</th>";
    sHTML += "<th>Date</th>";
    sHTML += "</tr>";
    sHTML += "</thead>";
    sHTML += "<tbody>";

    // pour chaque ligne de résultat, construire la ligne dans datatable
    for(i = 0; i < aOfOrders.length; i++) {

        sHTML += '<tr style="text-align:center;">';

        sHTML += '<th scope="row" style="text-align: center;"><a href="https://www.pincab.eu/admin797xdo6aw/index.php?controller=AdminOrders&vieworder=&id_order=' + aOfOrders[i]["id_order"] + '&token=' + tokenAdminOrders + '" ' + 'target="_blank">' + aOfOrders[i]["id_order"] + '</a></th>';

        link = aOfOrders[i]["url"].replace("@", aOfOrders[i]["tracking_number"]);

        sHTML += '<td><a href="' + link + '" target="_blank">' + aOfOrders[i]["tracking_number"] + '</a></td>';

        if (aOfOrders[i]["current_state"] == 1) {
            sHTML += `<td>En attente du paiement par chèque</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 2) {
            sHTML += `<td>Paiement accepté</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 3) {
            sHTML += `<td>En cours de préparation</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 4) {
            sHTML += '<td>Expédié';
            sHTML += '<button class="btn btn-danger" onclick="updateStatus(' + i + ')" class="red" title="Changer le statut" style="margin-left: 10px;">Changer le statut en : Livré</button>';
            sHTML += '</td>';
        }
        else if (aOfOrders[i]["current_state"] == 5) {
            sHTML += `<td class="green" style="font-weight:bold;">Livré</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 6) {
            sHTML += `<td>Annulé</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 7) {
            sHTML += `<td>Remboursé</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 8) {
            sHTML += `<td>Erreur de paiement</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 9) {
            sHTML += `<td>En attente de réapprovisionnement (payé)</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 10) {
            sHTML += `<td>En attente de virement bancaire</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 11) {
            sHTML += `<td>Paiement à distance accepté</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 12) {
            sHTML += `<td>En attente de réapprovisionnement (non payé)</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 13) {
            sHTML += `<td>En attente de paiement à la livraison</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 14) {
            sHTML += `<td>En attente de paiement PayPal</td>`;
        }
        else if (aOfOrders[i]["current_state"] == 15) {
            sHTML += `<td>Reliquat de commande à livrer</td>`;
        }

        sHTML += "<td>" + aOfOrders[i]["date_add"] + "</td>";

        sHTML += "</tr>";
    }
    sHTML += "</tbody>";
    sHTML += "</table>";
    $('#order_table').html(sHTML);
}

/**
 * clear HTML table
 * clear and destroy datatable
 * build table and call database
 */
function rebuildDatatableOrders() {
    tablesOrders.clear();
    tablesOrders.destroy();
    constructTableOrders();
    tablesOrders = $('#datatable').DataTable(configuration);
}

// datatable configuration
// order : tri selon une colonne (0 = 1ère colonne) par ordre croissant (asc) ou décroissant (desc)
// pageLength : nombre de résultats affichés par défaut
// lenghtMenu : choix du nombre de résultats à afficher
// language : traduction
// columns :
    // orderable : triable (true ou false)
    // visible : colonne affichée ou non (ID par exemple, ou date (si on veut trier par la date par exemple)) (true ou false)
    // searchable : filtrable par le champ de recherche
const configuration = {
    "stateSave": false,
    "order": [[3, "asc"]],
    "pageLength": 10,
    "pagingType": "simple_numbers",
    "searching": true,
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "Tous"]],
    "language" : {
        "info": "Affichage des lignes _START_ &agrave; _END_ sur _TOTAL_ r&eacute;sultats au total",
        "emptyTable": "Aucun r&eacute;sultat disponible",
        "lengthMenu": "Affichage de _MENU_ r&eacute;sultats",
        "search" : "Rechercher : ",
        "zeroRecords" : "Aucun r&eacute;sultat trouv&eacute;",
        "paginate" : {
            "previous": "Pr&eacute;c&eacute;dent",
            "next": "Suivant"
        },
        "sInfoFiltered": "(filtr&eacutes &agrave; partir de _MAX_ r&eacute;sultats au total)",
        "sInfoEmpty": "Aucun r&eacute;sultat disponible"
    },
    "dom": 'flrtip',
    "columns": [
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": true
        },
        {
            "orderable": false,
            "visible": false,
            "searchable": false
        }
    ],
    "retrieve": true,
    "responsive": true,
    "autoWidth": false
};

/**
 * Init start
 *
 */

// une fois la page chargée, charger les commandes
$(document).ready(function() {
    loadTokenAdminOrders();
    loadOrders();
});