<?php
require_once "cron_sales_stats_service.php";

/**
 * Class Update_film | file Update_film.php
 *
 * In this class, we show the interface "Update_film.html".
 * With this interface, we'll be able to update a movie with its id
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */
class Cron_sales_stats_update_duration_three	{
	
	/**
	 * public $resultat is used to store all datas needed for HTML Templates
	 * @var array
	 */
	public $resultat;

	/**
	 * init variables resultat
	 *
	 * execute main function
	 */
	public function __construct()	{
		// init variables resultat
		$this->resultat= [];

		// execute main function
		$this->main();
	}

	/**
	 * Update a movie with its id
	 */
	function main()	{
		$objet_cron_sales_stats_update_duration_three = new Cron_sales_stats_service();
		$objet_cron_sales_stats_update_duration_three->cron_sales_stats_update_duration_three();

		$this->resultat= $objet_cron_sales_stats_update_duration_three->resultat;
		$this->VARS_HTML= $objet_cron_sales_stats_update_duration_three->VARS_HTML;
	}
}
?>
