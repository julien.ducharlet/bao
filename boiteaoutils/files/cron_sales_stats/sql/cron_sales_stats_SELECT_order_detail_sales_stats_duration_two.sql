SELECT DB_PREFIX_order_detail.product_id, DB_PREFIX_order_detail.product_attribute_id, SUM(DB_PREFIX_order_detail.product_quantity) AS 'sales_stats_duration_2'
FROM DB_PREFIX_order_detail
LEFT JOIN DB_PREFIX_product ON DB_PREFIX_order_detail.product_id = DB_PREFIX_product.id_product
LEFT JOIN DB_PREFIX_orders ON DB_PREFIX_order_detail.id_order = DB_PREFIX_orders.id_order
WHERE DATEDIFF(NOW(),DB_PREFIX_orders.invoice_date) <= 60
GROUP BY DB_PREFIX_order_detail.product_id, DB_PREFIX_order_detail.product_attribute_id
ORDER BY DB_PREFIX_order_detail.product_id, DB_PREFIX_order_detail.product_attribute_id;