var aOfProducts = [];

var aOfSalesStatsAll = [];

var aOfSalesStatsOne = [];

var aOfSalesStatsTwo = [];

var aOfSalesStatsThree = [];

function getProductsAndAttributes() {
    var datas = {
        path : "cron_sales_stats",
        page: "cron_sales_stats_products_and_attributes_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iProduct = 0;
            for (var ligne in result) {
                aOfProducts[iProduct] = [];
                aOfProducts[iProduct]["id_product"] = result[ligne]["id_product"];
                aOfProducts[iProduct]["id_product_attribute"] = result[ligne]["id_product_attribute"];
                aOfProducts[iProduct]["date_add"] = result[ligne]["date_add"];

                iProduct++;
            }
            updateProductAttributes(aOfProducts);
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function updateProductAttributes(aOfProductsToUpdate) {
    var success = 0;
    var error = 0;

    for (var i = 0; i < aOfProductsToUpdate.length; i++) {
        var datas = {
            path : "cron_sales_stats",
            page: "cron_sales_stats_update_date_add",
            bJSON: 1,
            'product_id': aOfProductsToUpdate[i]["id_product"],
            'product_attribute_id': aOfProductsToUpdate[i]["id_product_attribute"],
            'date_add': aOfProductsToUpdate[i]["date_add"]
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    if (error == 0) {
        getSalesStatsAll();
    }
}

function getSalesStatsAll() {
    var datas = {
        path : "cron_sales_stats",
        page: "cron_sales_stats_duration_all_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iSalesStatsAll = 0;
            for (var ligne in result) {

                aOfSalesStatsAll[iSalesStatsAll] = [];
                aOfSalesStatsAll[iSalesStatsAll]["product_id"] = result[ligne]["product_id"];
                aOfSalesStatsAll[iSalesStatsAll]["product_attribute_id"] = result[ligne]["product_attribute_id"];
                aOfSalesStatsAll[iSalesStatsAll]["sales_stats_duration_all"] = result[ligne]["sales_stats_duration_all"];

                iSalesStatsAll++;
            }
            updateSalesStatsAll(aOfSalesStatsAll);
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function updateSalesStatsAll(aOfSalesStatsAllToUpdate) {
    var success = 0;
    var error = 0;

    for (var i = 0; i < aOfSalesStatsAllToUpdate.length; i++) {
        var datas = {
            path : "cron_sales_stats",
            page: "cron_sales_stats_update_duration_all",
            bJSON: 1,
            'product_id': aOfSalesStatsAllToUpdate[i]["product_id"],
            'product_attribute_id': aOfSalesStatsAllToUpdate[i]["product_attribute_id"],
            'sales_stats_duration_all': aOfSalesStatsAllToUpdate[i]["sales_stats_duration_all"],
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    if (error == 0) {
        getSalesStatsOne();
    }
}

function getSalesStatsOne() {
    var success = 0;
    var error = 0;

    var datas = {
        path : "cron_sales_stats",
        page: "cron_sales_stats_duration_one_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iSalesStatsOne = 0;
            for (var ligne in result) {
                aOfSalesStatsOne[iSalesStatsOne] = [];
                aOfSalesStatsOne[iSalesStatsOne]["product_id"] = result[ligne]["product_id"];
                aOfSalesStatsOne[iSalesStatsOne]["product_attribute_id"] = result[ligne]["product_attribute_id"];
                aOfSalesStatsOne[iSalesStatsOne]["sales_stats_duration_1"] = result[ligne]["sales_stats_duration_1"];

                iSalesStatsOne++;
            }
            updateSalesStatsOne(aOfSalesStatsOne);
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function updateSalesStatsOne(aOfSalesStatsOneToUpdate) {
    var success = 0;
    var error = 0;

    for (var i = 0; i < aOfSalesStatsOneToUpdate.length; i++) {
        var datas = {
            path : "cron_sales_stats",
            page: "cron_sales_stats_update_duration_one",
            bJSON: 1,
            'product_id': aOfSalesStatsOneToUpdate[i]["product_id"],
            'product_attribute_id': aOfSalesStatsOneToUpdate[i]["product_attribute_id"],
            'sales_stats_duration_1': aOfSalesStatsOneToUpdate[i]["sales_stats_duration_1"],
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    if (error == 0) {
        getSalesStatsTwo();
    }
}

function getSalesStatsTwo() {
    var success = 0;
    var error = 0;

    var datas = {
        path : "cron_sales_stats",
        page: "cron_sales_stats_duration_two_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iSalesStatsTwo = 0;
            for (var ligne in result) {
                aOfSalesStatsTwo[iSalesStatsTwo] = [];
                aOfSalesStatsTwo[iSalesStatsTwo]["product_id"] = result[ligne]["product_id"];
                aOfSalesStatsTwo[iSalesStatsTwo]["product_attribute_id"] = result[ligne]["product_attribute_id"];
                aOfSalesStatsTwo[iSalesStatsTwo]["sales_stats_duration_2"] = result[ligne]["sales_stats_duration_2"];

                iSalesStatsTwo++;
            }
            updateSalesStatsTwo(aOfSalesStatsTwo);
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function updateSalesStatsTwo(aOfSalesStatsTwoToUpdate) {
    var success = 0;
    var error = 0;

    for (var i = 0; i < aOfSalesStatsTwoToUpdate.length; i++) {
        var datas = {
            path : "cron_sales_stats",
            page: "cron_sales_stats_update_duration_two",
            bJSON: 1,
            'product_id': aOfSalesStatsTwoToUpdate[i]["product_id"],
            'product_attribute_id': aOfSalesStatsTwoToUpdate[i]["product_attribute_id"],
            'sales_stats_duration_2': aOfSalesStatsTwoToUpdate[i]["sales_stats_duration_2"],
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    if (error == 0) {
        getSalesStatsThree();
    }
}

function getSalesStatsThree() {
    var success = 0;
    var error = 0;

    var datas = {
        path : "cron_sales_stats",
        page: "cron_sales_stats_duration_three_list",
        bJSON: 1
    }
    $.ajax({
        type: "POST",
        url: "route.php",
        async: true,
        data: datas,
        dataType: "json",
        cache: false
    })
        .done(function (result) {
            var iSalesStatsThree = 0;
            for (var ligne in result) {
                aOfSalesStatsThree[iSalesStatsThree] = [];
                aOfSalesStatsThree[iSalesStatsThree]["product_id"] = result[ligne]["product_id"];
                aOfSalesStatsThree[iSalesStatsThree]["product_attribute_id"] = result[ligne]["product_attribute_id"];
                aOfSalesStatsThree[iSalesStatsThree]["sales_stats_duration_3"] = result[ligne]["sales_stats_duration_3"];

                iSalesStatsThree++;
            }
            updateSalesStatsThree(aOfSalesStatsThree);
        })
        .fail(function (err) {
            alert('error : ' + err.status);
        });
}

function updateSalesStatsThree(aOfSalesStatsThreeToUpdate) {
    var success = 0;
    var error = 0;

    for (var i = 0; i < aOfSalesStatsThreeToUpdate.length; i++) {
        var datas = {
            path : "cron_sales_stats",
            page: "cron_sales_stats_update_duration_three",
            bJSON: 1,
            'product_id': aOfSalesStatsThreeToUpdate[i]["product_id"],
            'product_attribute_id': aOfSalesStatsThreeToUpdate[i]["product_attribute_id"],
            'sales_stats_duration_3': aOfSalesStatsThreeToUpdate[i]["sales_stats_duration_3"],
        }
        $.ajax({
            type: "POST",
            url: "route.php",
            async: true,
            data: datas,
            dataType: "json",
            cache: false
        })
            .done(function (result) {
                success++;
            })
            .fail(function (err) {
                error++;
            });
    }
    if (error == 0) {
        $('#divModalLoading').hide();
    }
}