<?php
/**
 * Router | file route.php
 *
 * This is the main program of the project.
 * We must use this file, if we want to access of all others classes.
 *
 * @package Cinema Project
 * @subpackage configuration
 * @author @Afpa Lab Team
 * @copyright  1920-2080 The Afpa Lab Team Group Corporation World Company
 * @version v1.0
 */


/**
 * We get the content of the config file.
 */
require "configuration.php";
$GLOBALS_INI = Configuration::getGlobalsINI();
/**
 * We start the session
 */
session_start();
if (((isset($_GET["tkap"])) && ($_GET["tkap"] != "")) && ((!isset($_SESSION["token_admin_products"])) || ($_SESSION["token_admin_products"] == ""))) {
	$_SESSION['token_admin_products'] = $_GET['tkap'];
}
if (((isset($_GET["tkad"])) && ($_GET["tkad"] != "")) && ((!isset($_SESSION["token_admin_dashboard"])) || ($_SESSION["token_admin_dashboard"] == ""))) {
	$_SESSION['token_admin_dashboard'] = $_GET['tkad'];
}
if (((isset($_GET["tkao"])) && ($_GET["tkao"] != "")) && ((!isset($_SESSION["token_admin_orders"])) || ($_SESSION["token_admin_orders"] == ""))) {
	$_SESSION['token_admin_orders'] = $_GET['tkao'];
}
/**
 * We get the page parameter, needed to call the class.
 */

if ((isset($_GET["page"])) && ($_GET["page"] != ""))	{
	$monPHP = $_GET["page"];
}
else if ((isset($_POST["page"])) && ($_POST["page"] != ""))	{
    $monPHP = $_POST["page"];
}
else {
    $monPHP = "index";
}

if ((isset($_GET["path"])) && ($_GET["path"] != "")) {
	$monChemin = $_GET["path"] . "/";
}
else if ((isset($_POST["path"])) && ($_POST["path"] != "")) {
	$monChemin = $_POST["path"] . "/";
}
else {
    $monChemin = "includes" . "/";
}

/**
 * Test if classes exist.
 * If not, redirect to a specific class
 */
if (!(file_exists($GLOBALS_INI["PATH_HOME"] . 'files/' . $monChemin . 'php/' . $monPHP . ".php"))) {
	$monPHP = "index";
}

/**
 * Instantiation of the dynamic class
 */
$myClass = ucfirst($monPHP);
require $GLOBALS_INI["PATH_HOME"] . 'files/' . $monChemin . 'php/' . $monPHP . ".php";
$oMain = new $myClass();

/**
 * Call the view "route.html".
 * If parameter bJSON is passed and set to 1, we load another HTML page.
 */
$page_to_load= "route.html";
if ((isset($oMain->VARS_HTML["bJSON"])) && ($oMain->VARS_HTML["bJSON"] == 1))	{
	$page_to_load = $monPHP . ".html";
}

require $GLOBALS_INI["PATH_HOME"] . 'files/' . $monChemin . 'html/' . $page_to_load;

/**
 * Destroy $oMain object.
 */
unset($oMain);
?>




